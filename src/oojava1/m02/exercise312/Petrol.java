package oojava1.m02.exercise312;

public class Petrol {

	public static void main(String[] args) {
		PetrolPurchase pp = new PetrolPurchase("Tórshavn", "Bezin 98 Oktan", 42, 11.22, 15);
		System.out.printf("The purchase amount is %.2f kr.\n", pp.getPurchaseAmount());

	}

}
