package oojava1.m04.ex05_array;

public class ArrayDemo {
	public static void main(String[] args) {
		int N = 5;
		int a[] = new int[N];
		for (int i=0; i<N; i++) 
			a[i]=i*i;
		int i=0;
		System.out.printf("Value of variable                                : %d\t%d\t%d\t%d\t%d\n", a[i++],a[i++],a[i++],a[i++],a[i++]);
		incValue(a);
		i=0;
		System.out.printf("Value of variable outside method after increment : %d\t%d\t%d\t%d\t%d\n", a[i++],a[i++],a[i++],a[i++],a[i++]);
	}
	
	public static void incValue(int[] b) {
		b[1]=b[1]+100;
		b[3]=b[3]+100;
		int i=0;
		System.out.printf("Value of variable inside method after increment  : %d\t%d\t%d\t%d\t%d\n", b[i++],b[i++],b[i++],b[i++],b[i++]);
	}
}